package net.soevar.crudapi.presenter

import net.soevar.crudapi.BuildConfig
import net.soevar.crudapi.model.Barang
import net.soevar.crudapi.model.ResultBarang
import net.soevar.crudapi.model.ResultSimple
import net.soevar.crudapi.model.User
import net.soevar.crudapi.network.RetrofitClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BarangPresenter(val barangView: BarangView) {
    fun getBarang(user: User?) {
        RetrofitClient.service(BuildConfig.SERVER_URL)
            .getDataBarang(user?.idUser)
            .enqueue(object : Callback<ResultBarang> {
                override fun onFailure(call: Call<ResultBarang>, t: Throwable) {
                    barangView.onErrorBarang(t.localizedMessage)
                }

                override fun onResponse(call: Call<ResultBarang>, response: Response<ResultBarang>) {
                    val body = response.body()
                    if (body?.status == 200) {
                        barangView.onSuccessBarang(body.barang)
                    } else {
                        barangView.onErrorBarang(body?.message)
                    }
                }

            })

    }

    fun deleteBarang(barang: Barang) {
        RetrofitClient.service(BuildConfig.SERVER_URL)
            .deleteBarang(
                Integer.parseInt(barang.idUser),
                Integer.parseInt(barang.idBarang),
                barang.namaBarang
            )
            .enqueue(object : Callback<ResultSimple> {
                override fun onFailure(call: Call<ResultSimple>, t: Throwable) {
                    barangView.onSuccessDeleteBarang(t.localizedMessage)
                }

                override fun onResponse(call: Call<ResultSimple>, response: Response<ResultSimple>) {
                    val body = response.body()
                    if (body?.status == 200) {
                        barangView.onSuccessDeleteBarang(body?.message)
                    } else {
                        barangView.onErrorDeleteBarang(body?.message)
                    }
                }


            })
    }
}