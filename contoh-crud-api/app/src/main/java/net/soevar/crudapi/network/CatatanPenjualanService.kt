package net.soevar.crudapi.network

import net.soevar.crudapi.model.ResultBarang
import net.soevar.crudapi.model.ResultSimple
import net.soevar.crudapi.model.ResultUser
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface CatatanPenjualanService {

    @FormUrlEncoded
    @POST("loginUser")
    fun loginUser(
        @Field("username") username: String,
        @Field("password") password: String
    ): Call<ResultUser>


    @FormUrlEncoded
    @POST("registerUser")
    fun registerUser(
        @Field("username") username: String?,
        @Field("email") email: String?,
        @Field("password") password: String?,
        @Field("hp") hp: String?
    ): Call<ResultSimple>

    @FormUrlEncoded
    @POST("getDataBarang")
    fun getDataBarang(@Field("id_user") idUser: Int?): Call<ResultBarang>

    @FormUrlEncoded
    @POST("addBarang")
    fun addBarang(
        @Field("id_user") idUser: Int?,
        @Field("barcode") barcode: String?,
        @Field("nama_barang") namaBarang: String?,
        @Field("kategori") kategori: String?,
        @Field("harga_beli") hargaBeli: Double?,
        @Field("harga_jual") hargaJual: Double?
    ): Call<ResultSimple>

    @FormUrlEncoded
    @POST("updateBarang")
    fun updateBarang(
        @Field("id_user") idUser: Int?,
        @Field("id_barang") idBarang: Int?,
        @Field("barcode") barcode: String?,
        @Field("nama_barang") namaBarang: String?,
        @Field("kategori") kategori: String?,
        @Field("harga_beli") hargaBeli: Double?,
        @Field("harga_jual") hargaJual: Double?
    ): Call<ResultSimple>

    @FormUrlEncoded
    @POST("deleteBarang")
    fun deleteBarang(
        @Field("id_user") idUser: Int?,
        @Field("id_barang") idBarang: Int?,
        @Field("nama_barang") namaBarang: String?
    ): Call<ResultSimple>
}
