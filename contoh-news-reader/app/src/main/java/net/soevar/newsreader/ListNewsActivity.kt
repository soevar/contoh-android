package net.soevar.newsreader

import android.app.AlertDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.squareup.picasso.Picasso
import dmax.dialog.SpotsDialog
import kotlinx.android.synthetic.main.activity_list_news.*
import net.soevar.newsreader.adapter.ListNewsAdapter
import net.soevar.newsreader.model.News
import net.soevar.newsreader.service.NewsService
import net.soevar.newsreader.util.Common
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ListNewsActivity : AppCompatActivity() {

    var source = ""
    var webToURL: String? = ""

    lateinit var dialog: AlertDialog
    lateinit var mService: NewsService
    lateinit var adapter: ListNewsAdapter
    //lateinit var layoutManager: LinearLayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_news)

        // init view
        mService = Common.newsService
        dialog = SpotsDialog.Builder().setContext(this).build()
        swipe_list_news.setOnRefreshListener { loadNews(source, true) }

        diagonalLayout.setOnClickListener {
            val detail = Intent(baseContext, NewsDetailActivity::class.java)
            detail.putExtra("webURL", webToURL)
            startActivity(detail)
        }

        listNews.setHasFixedSize(true)
        listNews.layoutManager = LinearLayoutManager(this)

        if (intent != null) {
            source = intent.getStringExtra("source")
            if (!source.isEmpty()) {
                loadNews(source, false)
            }
        }
    }

    private fun loadNews(source: String?, isRefreshed: Boolean) {
        if (isRefreshed) {
            dialog.show()
            mService.getNewsFromSource(Common.getNewsAPI(source!!))
                .enqueue(object : Callback<News> {
                    override fun onFailure(call: Call<News>, t: Throwable) {
                        swipe_list_news.isRefreshing = false
                    }

                    override fun onResponse(call: Call<News>, response: Response<News>) {
                        dialog.dismiss()

                        Picasso.get()
                            .load(response.body()!!.articles!![0].urlToImage)
                            .into(topImage)
                        topTitle.text = response.body()!!.articles!![0].title
                        topAuthor.text = response.body()!!.articles!![0].author

                        webToURL = response.body()!!.articles!![0].url

                        //load all remain articles
                        val removeFirstItem = response.body()!!.articles

                        //because we get first item to hot new, so we need remove it
                        removeFirstItem!!.removeAt(0)

                        adapter = ListNewsAdapter(removeFirstItem, baseContext)
                        adapter.notifyDataSetChanged()

                        listNews.adapter = adapter
                    }

                })
        } else {
            //dialog.show()
            swipe_list_news.isRefreshing = true
            mService.getNewsFromSource(Common.getNewsAPI(source!!))
                .enqueue(object : Callback<News> {
                    override fun onFailure(call: Call<News>, t: Throwable) {
                        //Toast.makeText(baseContext, "Terjadi error", Toast.LENGTH_SHORT).show()
                        swipe_list_news.isRefreshing = false
                    }

                    override fun onResponse(call: Call<News>, response: Response<News>) {
                        swipe_list_news.isRefreshing = false

                        Picasso.get()
                            .load(response.body()!!.articles!![0].urlToImage)
                            .into(topImage)
                        topTitle.text = response.body()!!.articles!![0].title
                        topAuthor.text = response.body()!!.articles!![0].author

                        webToURL = response.body()!!.articles!![0].url

                        //load all remain articles
                        val removeFirstItem = response.body()!!.articles

                        //because we get first item to hot new, so we need remove it
                        removeFirstItem!!.removeAt(0)

                        adapter = ListNewsAdapter(removeFirstItem, baseContext)
                        adapter.notifyDataSetChanged()

                        listNews.adapter = adapter
                    }

                })
        }
    }
}
