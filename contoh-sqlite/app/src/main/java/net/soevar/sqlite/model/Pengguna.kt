package net.soevar.sqlite.model

class Pengguna {
    var id: Int = 0
    var nama: String? = null
    var email: String? = null

    constructor() {}

    constructor(id: Int, nama: String, email: String) {
        this.id = id
        this.nama = nama
        this.email = email
    }
}