package net.soevar.newsreader.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import net.soevar.newsreader.ListNewsActivity
import net.soevar.newsreader.R
import net.soevar.newsreader.model.Website
import net.soevar.newsreader.service.ItemClickListener

class ListSourceAdapter(private val context: Context, private val website: Website) :
    RecyclerView.Adapter<ListSourceViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListSourceViewHolder {
        val inflater = LayoutInflater.from(parent!!.context)
        val itemView = inflater.inflate(R.layout.source_news_layout, parent, false)
        return ListSourceViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return website.sources!!.size
    }

    override fun onBindViewHolder(holder: ListSourceViewHolder, position: Int) {
        holder!!.source_title.text = website.sources!![position].name

        holder.setItemClickListener(object : ItemClickListener {
            override fun onClick(view: View, position: Int) {
                //Toast.makeText(context, website.sources!![position].id, Toast.LENGTH_SHORT).show()
                val intent = Intent(context, ListNewsActivity::class.java)
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("source", website.sources!![position].id)
                context.startActivity(intent)
            }

        })
    }
}