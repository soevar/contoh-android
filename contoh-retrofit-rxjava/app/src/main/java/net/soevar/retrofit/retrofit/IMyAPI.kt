package net.soevar.retrofit.retrofit

import io.reactivex.Observable
import net.soevar.retrofit.model.Post
import retrofit2.http.GET

interface IMyAPI {

    @get:GET("posts")
    val posts: Observable<List<Post>>
}