package net.soevar.crudapi.presenter

import net.soevar.crudapi.BuildConfig
import net.soevar.crudapi.model.ResultSimple
import net.soevar.crudapi.model.User
import net.soevar.crudapi.network.RetrofitClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RegisterPresenter(val registerView: RegisterView) {
    fun register(user: User) {
        RetrofitClient.service(BuildConfig.SERVER_URL)
            .registerUser(user.username, user.email, user.password, user.hp)
            .enqueue(object : Callback<ResultSimple> {
                override fun onFailure(call: Call<ResultSimple>, t: Throwable) {
                    registerView.onErrorRegister(t.localizedMessage)
                }

                override fun onResponse(call: Call<ResultSimple>, response: Response<ResultSimple>) {
                    if (response.body()?.status == 200) {
                        registerView.onSuccessRegister()
                    } else {
                        registerView.onErrorRegister(response.body()?.message)
                    }
                }

            })
    }
}