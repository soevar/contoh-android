package net.soevar.crudapi.utils

import java.text.NumberFormat
import java.util.*

object Uang {
    fun indonesia(uang: Double): String {
        val localId = Locale("in", "ID")
        val kursId: NumberFormat = NumberFormat.getCurrencyInstance(localId)

        return kursId.format(uang)
    }
}