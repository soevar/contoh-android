package net.soevar.newsreader.service

import net.soevar.newsreader.model.News
import net.soevar.newsreader.model.Website
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Url

interface NewsService {
    @get:GET("v2/sources?apiKey=d776a107c5724338acc65c60b1f01f08")
    val sources: Call<Website>

    @GET
    fun getNewsFromSource(@Url url: String): Call<News>
}