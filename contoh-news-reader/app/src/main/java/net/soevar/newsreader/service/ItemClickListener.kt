package net.soevar.newsreader.service

import android.view.View

interface ItemClickListener {
    fun onClick(view: View, position: Int)
}