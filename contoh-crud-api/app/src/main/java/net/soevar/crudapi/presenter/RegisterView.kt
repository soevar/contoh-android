package net.soevar.crudapi.presenter

import net.soevar.crudapi.model.User

interface RegisterView {
    fun onSuccessRegister()
    fun onErrorRegister(msg: String?)
}