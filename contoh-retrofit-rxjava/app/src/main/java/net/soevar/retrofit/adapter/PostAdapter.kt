package net.soevar.retrofit.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import net.soevar.retrofit.R
import net.soevar.retrofit.model.Post
import java.lang.StringBuilder

class PostAdapter(internal var context: Context, internal var postList: List<Post>) :
    RecyclerView.Adapter<PostViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.post_layout, parent, false)

        return PostViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return postList.size ?: 0
    }

    override fun onBindViewHolder(holder: PostViewHolder, position: Int) {
        Log.d("user", postList[position].userID.toString())
        holder.txtAuthor.text = postList[position].userID.toString()
        holder.txtTitle.text = postList[position].title
        holder.txtContent.text = StringBuilder(postList[position].body.substring(0, 20))
            .append("...").toString()
    }
}