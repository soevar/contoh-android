package net.soevar.crudapi.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Barang(
    @field:SerializedName("id_barang") var idBarang: String? = null,

    @field:SerializedName("id_user") var idUser: String? = null,

    @field:SerializedName("barcode") var barcode: String? = null,

    @field:SerializedName("nama_barang") var namaBarang: String? = null,

    @field:SerializedName("kategori") var kategori: String? = null,

    @field:SerializedName("harga_jual") var hargaJual: Double? = null,

    @field:SerializedName("harga_beli") var hargaBeli: Double? = null,

    @field:SerializedName("stok") var stok: String? = null,

    @field:SerializedName("created_date") var createDate: String? = null
) : Serializable
