package net.soevar.newsreader.util

import net.soevar.newsreader.remote.RetrofitClient
import net.soevar.newsreader.service.NewsService
import java.lang.StringBuilder

object Common {
    val BASE_URL = "https://newsapi.org/"
    val API_KEY = "d776a107c5724338acc65c60b1f01f08"


    val newsService: NewsService
        get() = RetrofitClient.getClient(BASE_URL).create(NewsService::class.java)

    fun getNewsAPI(source: String): String {
        val apiURL = StringBuilder("https://newsapi.org/v2/top-headlines?sources=")
            .append(source)
            .append("&apiKey=")
            .append(API_KEY)
            .toString()

        return apiURL
    }
}