package net.soevar.crudapi.presenter

import net.soevar.crudapi.BuildConfig
import net.soevar.crudapi.model.ResultUser
import net.soevar.crudapi.model.User
import net.soevar.crudapi.network.RetrofitClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginPresenter(val loginView: LoginView) {
    fun login(user: User) {
        RetrofitClient.service(BuildConfig.SERVER_URL)
                .loginUser(user.username.toString(), user.password.toString())
                .enqueue(object : Callback<ResultUser> {
                    override fun onFailure(call: Call<ResultUser>, t: Throwable) {
                        loginView.onErrorLogin(t.localizedMessage)
                    }

                    override fun onResponse(call: Call<ResultUser>, response: Response<ResultUser>) {
                        val body = response.body()
                        if (body?.status == 200) {
                            loginView.onSuccessLogin(body.user)
                        } else {
                            loginView.onErrorLogin(body?.message)
                        }
                    }

                })

    }
}