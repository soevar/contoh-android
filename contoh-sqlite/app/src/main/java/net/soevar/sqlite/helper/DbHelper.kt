package net.soevar.sqlite.helper

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import net.soevar.sqlite.model.Pengguna

class DbHelper(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
    companion object {
        private val DATABASE_NAME = "pengguna.db"
        private val DATABASE_VERSION = 1

        //table
        private val TABLE_NAME = "Pengguna"
        private val COL_ID = "id"
        private val COL_NAMA = "nama"
        private val COL_EMAIL = "email"
    }

    override fun onCreate(db: SQLiteDatabase?) {
        val CREATE_TABLE_QUERY = ("CREATE TABLE $TABLE_NAME ($COL_ID INTEGER PRIMARY KEY, $COL_NAMA TEXT,  $COL_EMAIL TEXT)")
        Log.d("CREATE_TABLE_QUERY", CREATE_TABLE_QUERY)
        db!!.execSQL(CREATE_TABLE_QUERY)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db!!.execSQL("DROP TABLE IF EXISTS $TABLE_NAME")
        onCreate(db!!)
    }

    //CRUD
    val allPengguna: List<Pengguna>
        get() {
            val listPengguna = ArrayList<Pengguna>()
            val selectQuery = "SELECT * FROM $TABLE_NAME"
            val db = this.writableDatabase
            val cursor = db.rawQuery(selectQuery, null)
            if (cursor.moveToFirst()) {
                do {
                    val pengguna = Pengguna()
                    pengguna.id = cursor.getInt(cursor.getColumnIndex(COL_ID))
                    pengguna.nama = cursor.getString(cursor.getColumnIndex(COL_NAMA))
                    pengguna.email = cursor.getString(cursor.getColumnIndex(COL_EMAIL))

                    listPengguna.add(pengguna)
                } while (cursor.moveToNext())
            }
            db.close()

            return listPengguna
        }

    fun addPengguna(pengguna: Pengguna) {
        val db = this.writableDatabase
        val values = ContentValues()
        values.put(COL_ID, pengguna.id)
        values.put(COL_NAMA, pengguna.nama)
        values.put(COL_EMAIL, pengguna.email)
        Log.d("values", values.toString())

        db.insert(TABLE_NAME, null, values)
        db.close()
    }

    fun updatePengguna(pengguna: Pengguna): Int {
        val db = this.writableDatabase
        val values = ContentValues()
        values.put(COL_ID, pengguna.id)
        values.put(COL_NAMA, pengguna.nama)
        values.put(COL_EMAIL, pengguna.email)

        return db.update(TABLE_NAME, values, "$COL_ID=?", arrayOf(pengguna.id.toString()))
    }

    fun deletePengguna(pengguna: Pengguna) {
        val db = this.writableDatabase

        db.delete(TABLE_NAME, "$COL_ID=?", arrayOf(pengguna.id.toString()))
        db.close()
    }
}
