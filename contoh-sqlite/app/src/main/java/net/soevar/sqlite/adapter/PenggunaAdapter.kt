package net.soevar.sqlite.adapter

import android.app.Activity
import android.app.Person
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.EditText
import kotlinx.android.synthetic.main.row_layout.view.*
import net.soevar.sqlite.R
import net.soevar.sqlite.model.Pengguna

class PenggunaAdapter(internal var activity: Activity,
                      internal var listPerson: List<Pengguna>,
                      internal var etId: EditText,
                      internal var etNama: EditText,
                      internal var etEmail: EditText) : BaseAdapter() {

    internal var inflater: LayoutInflater

    init {
        inflater = activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }


    override fun getView(position: Int, p1: View?, p2: ViewGroup?): View {
        val rowView: View
        rowView = inflater.inflate(R.layout.row_layout, null)

        rowView.txtRowId.text = listPerson[position].id.toString()
        rowView.txtNama.text = listPerson[position].nama.toString()
        rowView.txtEmail.text = listPerson[position].email.toString()

        //event
        rowView.setOnClickListener {
            etId.setText(rowView.txtRowId.text.toString())
            etNama.setText(rowView.txtNama.text.toString())
            etEmail.setText(rowView.txtEmail.text.toString())
        }
        return rowView
    }

    override fun getItem(position: Int): Any {
        return listPerson[position]
    }

    override fun getItemId(position: Int): Long {
        return listPerson[position].id.toLong()
    }

    override fun getCount(): Int {
        return listPerson.size
    }
}