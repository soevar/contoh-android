package net.soevar.crudapi.model

data class ResultUser(
        val status: Int? = null,
        val message: String? = null,
        val user: User? = null

)