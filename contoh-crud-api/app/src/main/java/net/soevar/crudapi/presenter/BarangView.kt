package net.soevar.crudapi.presenter

import net.soevar.crudapi.model.Barang

interface BarangView {
    fun onSuccessBarang(barang: List<Barang?>?)
    fun onErrorBarang(msg: String?)

    fun onSuccessDeleteBarang(msg: String?)
    fun onErrorDeleteBarang(msg: String?)
}