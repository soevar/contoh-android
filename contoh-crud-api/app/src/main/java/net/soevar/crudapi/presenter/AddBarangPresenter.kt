package net.soevar.crudapi.presenter

import net.soevar.crudapi.BuildConfig
import net.soevar.crudapi.model.Barang
import net.soevar.crudapi.model.ResultBarang
import net.soevar.crudapi.model.ResultSimple
import net.soevar.crudapi.model.User
import net.soevar.crudapi.network.RetrofitClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AddBarangPresenter(val addBarangView: AddBarangView) {
    fun updateBarang(barang: Barang) {
        RetrofitClient.service(BuildConfig.SERVER_URL)
            .updateBarang(
                Integer.parseInt(barang.idUser),
                Integer.parseInt(barang.idBarang),
                barang.barcode,
                barang.namaBarang,
                barang.kategori,
                barang.hargaBeli,
                barang.hargaJual
            )
            .enqueue(object : Callback<ResultSimple> {
                override fun onFailure(call: Call<ResultSimple>, t: Throwable) {
                    addBarangView.onErrorAddBarang(t.localizedMessage)
                }

                override fun onResponse(call: Call<ResultSimple>, response: Response<ResultSimple>) {
                    val body = response.body()
                    if (body?.status == 200) {
                        addBarangView.onSuccessAddBarang(body?.message)
                    } else {
                        addBarangView.onErrorAddBarang(body?.message)
                    }
                }

            })
    }

    fun addBarang(barang: Barang) {
        RetrofitClient.service(BuildConfig.SERVER_URL)
            .addBarang(
                Integer.parseInt(barang.idUser),
                barang.barcode,
                barang.namaBarang,
                barang.kategori,
                barang.hargaBeli,
                barang.hargaJual
            )
            .enqueue(object : Callback<ResultSimple> {
                override fun onFailure(call: Call<ResultSimple>, t: Throwable) {
                    addBarangView.onErrorAddBarang(t.localizedMessage)
                }

                override fun onResponse(call: Call<ResultSimple>, response: Response<ResultSimple>) {
                    val body = response.body()
                    if (body?.status == 200) {
                        addBarangView.onSuccessAddBarang(body?.message)
                    } else {
                        addBarangView.onErrorAddBarang(body?.message)
                    }
                }

            })
    }

    fun deleteBarang(barang: Barang) {
        RetrofitClient.service(BuildConfig.SERVER_URL)
            .deleteBarang(
                Integer.parseInt(barang.idUser),
                Integer.parseInt(barang.idBarang),
                barang.namaBarang
            )
            .enqueue(object : Callback<ResultSimple> {
                override fun onFailure(call: Call<ResultSimple>, t: Throwable) {
                    addBarangView.onErrorAddBarang(t.localizedMessage)
                }

                override fun onResponse(call: Call<ResultSimple>, response: Response<ResultSimple>) {
                    val body = response.body()
                    if (body?.status == 200) {
                        addBarangView.onSuccessAddBarang(body?.message)
                    } else {
                        addBarangView.onErrorAddBarang(body?.message)
                    }
                }

            })
    }
}