package net.soevar.crudapi.activity

import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_add_barang.*
import net.soevar.crudapi.R
import net.soevar.crudapi.base.BaseActivity
import net.soevar.crudapi.model.Barang
import net.soevar.crudapi.presenter.AddBarangPresenter
import net.soevar.crudapi.presenter.AddBarangView
import org.jetbrains.anko.toast
import java.io.Serializable

class AddBarangActivity : BaseActivity(), AddBarangView {

    override fun onCreate(savedInstanceState: Bundle?) {
        cekSesi(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_barang)

        val intent = intent.getSerializableExtra(TAGS.BARANG)

        if (intent != null) {
            setActionEditButton(intent)
        } else {
            setActionTambahButton()
        }
    }

    private fun setActionEditButton(serializable: Serializable) {
        btAddBarang.text = "Simpan"
        val brg = serializable as Barang
        etAddBarangBarcode.setText(brg.barcode)
        etAddBarangNamaBarang.setText(brg.namaBarang)
        etAddBarangKategori.setText(brg.kategori)
        etAddBarangHargaBeli.setText(brg.hargaBeli.toString())
        etAddBarangHargaJual.setText(brg.hargaJual.toString())

        btAddBarang.setOnClickListener {
            val barcode = etAddBarangBarcode.text.toString()
            val namaBarang = etAddBarangNamaBarang.text.toString()
            val kategori = etAddBarangKategori.text.toString()
            val hargaBeli = etAddBarangHargaBeli.text.toString()
            val hargaJual = etAddBarangHargaJual.text.toString()

            if (hargaBeli.isNotBlank() && hargaJual.isNotBlank()) {
                val hargaBeliTemp = hargaBeli.toDouble()
                val hargaJualTemp = hargaJual.toDouble()

                // buat objek kosong
                brg.idUser = user?.idUser.toString()
                brg.barcode = barcode
                brg.namaBarang = namaBarang.toUpperCase()
                brg.kategori = kategori.toLowerCase().capitalize()
                brg.hargaBeli = hargaBeliTemp
                brg.hargaJual = hargaJualTemp

                // simpan ke database
                AddBarangPresenter(this@AddBarangActivity).updateBarang(brg)
            } else {
                // jangan input
                Snackbar.make(it, "Harga tidak boleh kosong", Snackbar.LENGTH_SHORT)
            }
        }
    }

    private fun setActionTambahButton() {
        btAddBarang.setOnClickListener {
            btAddBarang.text = "Tambah"

            val barcode = etAddBarangBarcode.text.toString()
            val namaBarang = etAddBarangNamaBarang.text.toString()
            val kategori = etAddBarangKategori.text.toString()
            val hargaBeli = etAddBarangHargaBeli.text.toString()
            val hargaJual = etAddBarangHargaJual.text.toString()

            if (hargaBeli.isNotBlank() && hargaJual.isNotBlank()) {
                val hargaBeliTemp = hargaBeli.toDouble()
                val hargaJualTemp = hargaJual.toDouble()

                // buat objek kosong
                val brg = Barang()
                brg.idUser = user?.idUser.toString()
                brg.barcode = barcode
                brg.namaBarang = namaBarang.toUpperCase()
                brg.kategori = kategori.toLowerCase().capitalize()
                brg.hargaBeli = hargaBeliTemp
                brg.hargaJual = hargaJualTemp

                // simpan ke database
                AddBarangPresenter(this@AddBarangActivity).addBarang(brg)
            } else {
                // jangan input
                Snackbar.make(it, "Harga tidak boleh kosong", Snackbar.LENGTH_SHORT)
            }
        }
    }

    override fun onSuccessAddBarang(msg: String?) {
        toast(msg ?: "").show()
        finish()
    }

    override fun onErrorAddBarang(msg: String?) {
        toast(msg ?: "").show()
    }
}
