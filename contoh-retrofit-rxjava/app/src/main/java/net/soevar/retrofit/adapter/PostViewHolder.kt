package net.soevar.retrofit.adapter

import android.view.View
import androidx.appcompat.view.menu.ActionMenuItemView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.post_layout.view.*

class PostViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val txtAuthor = itemView.txtAuthor
    val txtTitle = itemView.txtTitle
    val txtContent = itemView.txtContent
}