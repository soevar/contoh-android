package net.soevar.crudapi.activity

import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_main.*
import net.soevar.crudapi.R
import net.soevar.crudapi.adapter.MenuAdapter
import net.soevar.crudapi.base.BaseActivity
import org.jetbrains.anko.startActivity

class MainActivity : BaseActivity() {
    private val TAG = MainActivity::class.java.simpleName

    override fun onCreate(savedInstanceState: Bundle?) {
        cekSesi(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initActionButton()
        tvWelcome.text = "Welcome\n${user?.username}"
    }

    private fun initActionButton() {
        mainMenu.adapter = MenuAdapter(object : MenuAdapter.OnMenuClick {
            override fun onClik(image: Int) {
                when (image) {
                    R.drawable.ic_goods -> openDataBarang()
                    R.drawable.ic_shopping_cart -> openDataPenjualan()
                    R.drawable.ic_report -> openDataLaporan()
                }
            }
        })
    }

    private fun openDataLaporan() {
        Log.d(TAG, "Data laporan")
    }

    private fun openDataPenjualan() {
        Log.d(TAG, "Data penjualan")
    }

    private fun openDataBarang() {
//        Log.d(TAG, "Data barang")
        startActivity<BarangActivity>(BaseActivity.TAGS.USER to user)
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        finish()
        startActivity<LoginActivity>()
        return super.onOptionsItemSelected(item)
    }
}
