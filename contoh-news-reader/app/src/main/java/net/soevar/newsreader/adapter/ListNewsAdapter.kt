package net.soevar.newsreader.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import net.soevar.newsreader.NewsDetailActivity
import net.soevar.newsreader.R
import net.soevar.newsreader.model.Article
import net.soevar.newsreader.service.ItemClickListener
import net.soevar.newsreader.util.ISO8601Parser
import java.text.ParseException
import java.util.*

class ListNewsAdapter(val articleList: List<Article>, private val context: Context) :
    RecyclerView.Adapter<ListNewsViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListNewsViewHolder {
        val inflater = LayoutInflater.from(parent!!.context)
        val itemView = inflater.inflate(R.layout.news_layout, parent, false)

        return ListNewsViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return articleList.size
    }

    //load image
    override fun onBindViewHolder(holder: ListNewsViewHolder, position: Int) {
        Picasso.get()
            .load(articleList[position]!!.urlToImage)
            .into(holder.articleImage)

        if (articleList[position].title!!.length > 65) {
            holder.articleTitle.text = articleList[position].title!!.substring(0, 65) + "..."
        } else {
            holder.articleTitle.text = articleList[position].title!!
        }

        if (articleList[position].publishedAt != null) {
            var date: Date? = null

            try {
                date = ISO8601Parser.parse(articleList[position].publishedAt!!)
            } catch (ex: ParseException) {
                ex.printStackTrace()
            }

            holder.articleTime.setReferenceTime(date!!.time)
        }

        // set event click
        holder.setItemClickListener(object : ItemClickListener {
            override fun onClick(view: View, position: Int) {
                val detail = Intent(context, NewsDetailActivity::class.java)
                detail.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                detail.putExtra("webURL", articleList[position].url)
                context.startActivity(detail)
            }

        })


    }
}