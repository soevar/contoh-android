package net.soevar.newsreader

import android.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import dmax.dialog.SpotsDialog
import io.paperdb.Paper
import kotlinx.android.synthetic.main.activity_main.*
import net.soevar.newsreader.adapter.ListSourceAdapter
import net.soevar.newsreader.model.Website
import net.soevar.newsreader.service.NewsService
import net.soevar.newsreader.util.Common
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    lateinit var layoutManager: LinearLayoutManager
    lateinit var mService: NewsService
    lateinit var adapter: ListSourceAdapter
    lateinit var dialog: AlertDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // init cache db
        Paper.init(this)

        // init service
        mService = Common.newsService

        // init view
        swipe_to_refresh.setOnRefreshListener {
            loadWebsiteSource(true)
        }

        rv_source_news.setHasFixedSize(true)
        layoutManager = LinearLayoutManager(this)
        rv_source_news.layoutManager = layoutManager

        dialog = SpotsDialog.Builder().setContext(this).build()
//        SpotsDialog(this)

        loadWebsiteSource(false)
    }

    private fun loadWebsiteSource(isRefresh: Boolean) {
        if (!isRefresh) {
            val cache = Paper.book().read<String>("cache")
            if (cache != null && !cache.isBlank() && cache != "null") {
                // read cache
                val website = Gson().fromJson<Website>(cache, Website::class.java)
                adapter = ListSourceAdapter(baseContext, website)
                adapter.notifyDataSetChanged()
                rv_source_news.adapter = adapter
            } else {
                // load website and write cache
                dialog.show()
                //fetch new data
                mService.sources.enqueue(object : Callback<Website> {
                    override fun onFailure(call: Call<Website>, t: Throwable) {
                        Toast.makeText(baseContext, "Failed", Toast.LENGTH_SHORT).show()
                    }

                    override fun onResponse(call: Call<Website>, response: Response<Website>) {
                        adapter = ListSourceAdapter(baseContext, response!!.body()!!)
                        adapter.notifyDataSetChanged()
                        rv_source_news.adapter = adapter

                        // save to cache
                        Paper.book().write("cache", Gson().toJson(response!!.body()!!))

                        dialog.dismiss()

                    }

                })
            }
        } else {
            swipe_to_refresh.isRefreshing = true
            //fetch new data
            mService.sources.enqueue(object : Callback<Website> {
                override fun onFailure(call: Call<Website>, t: Throwable) {
                    Toast.makeText(baseContext, "Failed", Toast.LENGTH_SHORT).show()
                }

                override fun onResponse(call: Call<Website>, response: Response<Website>) {
                    adapter = ListSourceAdapter(baseContext, response!!.body()!!)
                    adapter.notifyDataSetChanged()
                    rv_source_news.adapter = adapter

                    // save to cache
                    Paper.book().write("cache", Gson().toJson(response!!.body()!!))

                    swipe_to_refresh.isRefreshing = false

                }

            })
        }
    }
}
