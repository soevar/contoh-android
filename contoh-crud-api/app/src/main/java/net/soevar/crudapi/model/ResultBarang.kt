package net.soevar.crudapi.model

data class ResultBarang(
    val status: Int? = null,
    val message: String? = null,
    val barang: List<Barang>? = null

)