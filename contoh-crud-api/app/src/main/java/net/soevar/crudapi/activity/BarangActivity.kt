package net.soevar.crudapi.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_barang.*
import net.soevar.crudapi.R
import net.soevar.crudapi.adapter.BarangAdapter
import net.soevar.crudapi.base.BaseActivity
import net.soevar.crudapi.model.Barang
import net.soevar.crudapi.presenter.AddBarangPresenter
import net.soevar.crudapi.presenter.BarangPresenter
import net.soevar.crudapi.presenter.BarangView
import org.jetbrains.anko.startActivity

class BarangActivity : BaseActivity(), BarangView {
    private val TAG = BarangActivity::class.java.simpleName

    override fun onCreate(savedInstanceState: Bundle?) {
        cekSesi(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_barang)

        refreshBarang()

        btAddDataBarang.setOnClickListener {
            //startActivity<MainActivity>(BaseActivity.TAGS.USER to user)
            startActivity<AddBarangActivity>(BaseActivity.TAGS.USER to user);
        }
    }

    private fun refreshBarang() {
        BarangPresenter(this).getBarang(user)
    }

    override fun onSuccessBarang(barang: List<Barang?>?) {
        rvDataBarang.adapter = BarangAdapter(barang, object : BarangAdapter.OnMenuClicked {
            override fun onClik(meneItem: MenuItem, barang: Barang?) {
                when (meneItem.itemId) {
                    R.id.editBarang -> editBarang(barang)
                    R.id.hapusBarang -> hapusBarang(barang)
                }
            }

        })
    }

    private fun editBarang(barang: Barang?) {
        //Log.d(TAG, "editBarang : ${barang?.idBarang}")
        val intent = Intent(this, AddBarangActivity::class.java)
        intent.putExtra(TAGS.USER, user)
        intent.putExtra(TAGS.BARANG, barang)

        startActivityForResult(intent, 1)
    }

    private fun hapusBarang(barang: Barang?) {
        Log.d(TAG, "hapusBarang : ${barang?.idBarang}")
//        val intent = Intent(this, AddBarangActivity::class.java)
//        intent.putExtra(TAGS.USER, user)
//        intent.putExtra(TAGS.BARANG, barang)

        //startActivityForResult(intent, 2)
        //AddBarangPresenter(this@AddBarangActivity).updateBarang(brg)
        //AddBarangPresenter(this@AddBarangActivity).deleteBarang(barang)
        BarangPresenter(this@BarangActivity).deleteBarang(barang!!)

        refreshBarang()
    }

    override fun onErrorBarang(msg: String?) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }

    override fun onSuccessDeleteBarang(msg: String?) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }

    override fun onErrorDeleteBarang(msg: String?) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }

    override fun onResume() {
        super.onResume()
        refreshBarang()
    }
}
