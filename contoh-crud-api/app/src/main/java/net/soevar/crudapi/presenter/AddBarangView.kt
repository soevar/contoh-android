package net.soevar.crudapi.presenter

interface AddBarangView {
    fun onSuccessAddBarang(msg: String?)
    fun onErrorAddBarang(msg: String?)
}