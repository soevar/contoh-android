package net.soevar.crudapi.activity

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_login.*
import net.soevar.crudapi.R
import net.soevar.crudapi.presenter.LoginPresenter
import net.soevar.crudapi.base.BaseActivity
import net.soevar.crudapi.model.User
import net.soevar.crudapi.presenter.LoginView
import org.jetbrains.anko.toast
import org.jetbrains.anko.startActivity

class LoginActivity : AppCompatActivity(), LoginView {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        initActionButton()
    }

    private fun initActionButton() {
        btLogin.setOnClickListener {
            val user = User()
            user.username = etLoginUsername.text.toString()
            user.password = etLoginPassword.text.toString()

            Log.d("user", "username: ${user.username}, password ${user.password} ")

            LoginPresenter(this@LoginActivity).login(user)
        }
        goRegister.setOnClickListener {
            startActivity<RegisterActivity>()
        }
    }

    override fun onSuccessLogin(user: User?) {
        toast("Berhasil login").show()

        startActivity<MainActivity>(BaseActivity.TAGS.USER to user)
    }

    override fun onErrorLogin(msg: String?) {
        toast(msg ?: "").show()
    }
}
