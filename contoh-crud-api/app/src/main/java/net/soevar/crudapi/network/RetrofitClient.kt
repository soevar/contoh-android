package net.soevar.crudapi.network

import net.soevar.crudapi.model.User
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

object RetrofitClient {
    private var retrofit: Retrofit? = null

    fun getInterceptor(): OkHttpClient {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY

        return OkHttpClient().newBuilder()
                .addInterceptor(interceptor)
                .build()
    }

    fun getRetrofit(baseUrl: String?): Retrofit {
        if (retrofit == null) {
            retrofit = Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .client(getInterceptor())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
        }
        return retrofit!!
    }

   fun service(baseUrl: String?)= getRetrofit(baseUrl).create(CatatanPenjualanService::class.java)
    //fun service(baseUrl: String?) = getRetrofit(baseUrl).create(User::class.java)


}

//interface Users {
//    @GET("users")
//    fun getUsers(): Call<List<Response<User>>>
//}