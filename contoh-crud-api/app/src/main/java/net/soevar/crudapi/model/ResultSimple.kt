package net.soevar.crudapi.model

data class ResultSimple(
    val status: Int? = null,
    val message: String? = null
)
