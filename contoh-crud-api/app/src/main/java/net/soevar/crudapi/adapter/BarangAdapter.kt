package net.soevar.crudapi.adapter

import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_data_barang.view.*
import kotlinx.android.synthetic.main.item_menu.view.*
import net.soevar.crudapi.R
import net.soevar.crudapi.model.Barang

class BarangAdapter(val barang: List<Barang?>?, val onMenuClicked: OnMenuClicked) :
    RecyclerView.Adapter<BarangAdapter.MyHolder>() {

    class MyHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(barang: Barang?) {
            itemView.tvBarcode.text = barang?.barcode
            itemView.tvNamaBarang.text = barang?.namaBarang
            itemView.tvCategory.text = barang?.kategori
        }
    }

    interface OnMenuClicked {
        fun onClik(menuItem: MenuItem, barang: Barang?)
        //fun onDelete(menuItem: MenuItem, barang: Barang?)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_data_barang, parent, false)

        return MyHolder(view)
    }

    override fun getItemCount(): Int {
        return barang?.size ?: 0
    }

    override fun onBindViewHolder(holder: MyHolder, position: Int) {
        holder.bind(barang?.get(position))

        holder.itemView.ivMenuBarang.setOnClickListener {
            val popupMenu = PopupMenu(holder.itemView.context, it)
            popupMenu.menuInflater.inflate(R.menu.menu_barang, popupMenu.menu)
            popupMenu.show()

            popupMenu.setOnMenuItemClickListener {
                onMenuClicked.onClik(it, barang?.get(position))
                true
            }
        }
    }
}

