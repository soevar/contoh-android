package net.soevar.crudapi.presenter

import net.soevar.crudapi.model.User

interface LoginView {
    fun onSuccessLogin(user: User?)
    fun onErrorLogin(msg: String?)
}