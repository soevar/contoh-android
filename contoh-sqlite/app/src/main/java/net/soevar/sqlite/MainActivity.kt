package net.soevar.sqlite


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.row_layout.*
import net.soevar.sqlite.adapter.PenggunaAdapter
import net.soevar.sqlite.helper.DbHelper
import net.soevar.sqlite.model.Pengguna

class MainActivity : AppCompatActivity() {

    internal lateinit var db: DbHelper
    internal var listPengguna: List<Pengguna> = ArrayList<Pengguna>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        db = DbHelper(this)
        refreshData()

        // event
        btnAdd.setOnClickListener {
            val pengguna = Pengguna(
                    Integer.parseInt(etId.text.toString()),
                    etNama.text.toString(),
                    etEmail.text.toString())
            //1Log.d("pengguna", pengguna.nama)
            db.addPengguna(pengguna)
            refreshData()
        }
        btnUpdate.setOnClickListener {
            val pengguna = Pengguna(
                    Integer.parseInt(etId.text.toString()),
                    etNama.text.toString(),
                    etEmail.text.toString())
            db.updatePengguna(pengguna)
            refreshData()
        }
        btnDelete.setOnClickListener {
            val pengguna = Pengguna(
                    Integer.parseInt(etId.text.toString()),
                    etNama.text.toString(),
                    etEmail.text.toString())
            db.deletePengguna(pengguna)
            refreshData()
        }
    }

    private fun refreshData() {
        listPengguna = db.allPengguna
        val adapter = PenggunaAdapter(this@MainActivity, listPengguna, etId, etNama, etEmail)
        lvPengguna.adapter = adapter
    }
}
