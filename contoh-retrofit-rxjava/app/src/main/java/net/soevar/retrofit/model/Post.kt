package net.soevar.retrofit.model

class Post {
    var userID: Int = 0
    var id: Int = 0
    var title: String = ""
    var body: String = ""
}